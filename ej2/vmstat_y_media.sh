#!/bin/bash
dat=$(date +"%F")
hour=$(date +"%T")
frecuencia=$1
mue=$2
posCPUlibre=15

#posicionarse en el directorio en el que se guardaran los logs
cd ..
cd logs/

#vmstat cada intervalo dado por la $frecuencia y numero de muestras en $mue

vmstat $frecuencia $mue > log_vmstat_"$hour"_$dat.log

#generar fichero temporal solo con el % de CPU libre

tail -n $mue log_vmstat_"$hour"_$dat.log > temporal.dat

#calcular la media

sum=0
for index in `seq 2 $mue`
do
	head -$index temporal.dat | tail -1 > temporal2.dat
	let num=$(awk '{ print $'$posCPUlibre' }' temporal2.dat)
	let sum=$sum+$num
done

let mue=$mue-1
echo "scale=2; $sum / $mue" | bc > media_log_"$hour"_$dat.dat
rm temporal.dat temporal2.dat

#volver al directorio en el que estabamos
cd ..
cd ej2/
