#!/bin/bash

#variables de la monitorizacion
#	- nveces: numero de veces que ejecutaremos el script con el vmstat
#	- frec : segundos entre muestra y muestra
#	- nmuestras : numero de muestras que queremos por cada ejecucion del script
# 				  con el vmstat

nveces=24
frec=300
nmuestras=13

for index in `seq 1 $nveces`
do
	./vmstat_y_media.sh $frec $nmuestras
done
