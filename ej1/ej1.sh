#!/bin/bash

# Variables que usaremos durante la ejecucion
# 		- frec: frecuencia en segundo con la que tomaremos datos
#		- nveces: numero de muestras que queremos tomar

frec=300
nveces=7

top -b -d $frec -n $nveces | grep "KiB Mem:" | awk '{ print $3 - $5 }' > aux.dat
sum=0
for i in `seq 2 $nveces`
do
	let num=$(head -n $i aux.dat | tail -1 )
	let sum=$sum+$num
done

let nveces=$nveces-1
# hacer la media y guardarla en un archivo
echo "scale=2; $sum / $nveces" | bc > memoria_free.dat
rm aux.dat
